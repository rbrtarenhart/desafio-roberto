package com.roberto.desafio.repository;

import com.roberto.desafio.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.ArrayList;
import java.util.Optional;

public interface ProductRepository extends MongoRepository <Product, String> {
    Optional<Product> findByNameProduct(String nameProduct);
    ArrayList<Product> findAllByOrderByCreatedAtDesc();
}
